
package Clases;

public class AnualidadOrdinaria  extends ValorFuturo{

    public AnualidadOrdinaria(double valorPresente, double interesAnual, double periodos) {
        super(valorPresente, interesAnual, periodos);
    }

    @Override
    public double getValorFuturo() {
      double tmp = (super.valorPresente)*(((Math.pow((1+super.interesAnual), super.periodos))-1)/(super.interesAnual));
              return tmp;
    }
    
}
