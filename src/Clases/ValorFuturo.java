
package Clases;

public abstract class ValorFuturo {
    
    public double valorPresente, interesAnual, periodos;
    public static double pagosPorAño;
    
    public ValorFuturo(double valorPresente, double interesAnual, double periodos){
        this.valorPresente = valorPresente;
        this.interesAnual = interesAnual;
        this.periodos = periodos;
    }
    public abstract double getValorFuturo();
}
