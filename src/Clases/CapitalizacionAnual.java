
package Clases;

public class CapitalizacionAnual extends ValorFuturo {

    public CapitalizacionAnual(double valorPresente, double interesAnual, double periodos) {
        super(valorPresente, interesAnual, periodos);
    }

    @Override
    public double getValorFuturo() {
       double tmp = (super.valorPresente)*(Math.pow((1+super.interesAnual), super.periodos));
       return tmp;
    }
    
}
