
package Clases;

public class CapitalizacionNoAnual extends ValorFuturo{

    public CapitalizacionNoAnual(double valorPresente, double interesAnual, double periodos, double pagosPorAño) {
        super(valorPresente, interesAnual, periodos);
        ValorFuturo.pagosPorAño = pagosPorAño;
    }

    @Override
    public double getValorFuturo() {
     double i = super.interesAnual / ValorFuturo.pagosPorAño;
     double n = super.periodos * ValorFuturo.pagosPorAño;
     double tmp =(super.valorPresente)*Math.pow((1+i), n);
     return tmp;
    }
    
    
}
